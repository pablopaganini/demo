@extends('layouts.master')

@section('content')
    <div class="content" id="app">
      <ul v-if="showAdditionalIngredients">
        <li class="list-group-item">
          <div class="input-group mb-1">
            <div class="input-group-prepend">
              <button class="btn btn-outline-success" type="button" @click="addIngredient">Add</button>
            </div>
            <select class="custom-select" v-model="selectedIngredient">
              <option :value="-1" selected class="font-weight-bold">Choose another ingredient</option>
              <option v-for="(ingredient, inx) in availableIngredients" :key="ingredient.name" :value="inx">@{{ ingredient.name }} ($ @{{ ingredient.price }})</option>
            </select>
          </div>
        </li>
      </ul>
      <pizza-detail :pizza="pizza" :ingredientes="ingredientes" @remove_ingredient="removeIngredient($event)" @show_ingredients="showIngredients()" @buy="buy()"></pizza-detail>
    </div>
@endsection

@section('js')
    var app = new Vue({
        el: '#app',
        data: {
            pizza: @json($pizza),
            ingredientes: @json($pizza->ingredients),
            allIngredients: @json($ingredients),
            availableIngredients:[],
            showAdditionalIngredients: false,
            selectedIngredient: 0
        },
        created: function () {
          for(i = 0; i < this.allIngredients.length; i++) {
            for(j = 0; j < this.ingredientes.length; j++) {
              // console.log(this.ingredientes[j].id + ' - ' + this.allIngredients[i].id);
              if(this.ingredientes[j].id == this.allIngredients[i].id)
                break;
            }
            // console.log('ind: ' + j + ' largo: ' + this.ingredientes.length);
            if(j == this.ingredientes.length)
              this.availableIngredients.push(this.allIngredients[i]);
          }
        },
        methods: {
            removeIngredient: function(id) {
              this.availableIngredients.push(this.ingredientes[id]);
              this.ingredientes.splice(id, 1);
            },
            showIngredients: function() {
              if(this.availableIngredients.length)
                this.showAdditionalIngredients = true;
            },
            addIngredient: function() {
              if(this.selectedIngredient == -1)
                return;
              this.ingredientes.push(this.availableIngredients[this.selectedIngredient]);
              this.availableIngredients.splice(this.selectedIngredient, 1);
              if(this.availableIngredients.length == 0)
                this.showAdditionalIngredients = false;
            },
            buy: function() {
              for(ingredientes = [], i = 0; i < this.ingredientes.length; i++)
                ingredientes.push(this.ingredientes[i].id);
              
              this.$http.post('{{ route('order.store') }}',{'pizzaId': this.pizza.id, 'ingredientes': ingredientes}).then(response => {
                location.href = '{{ route('home') }}';
              }, response => {
                alert('post error!');
              });
            }
        }
    })
@endsection
