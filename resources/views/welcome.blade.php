@extends('layouts.master')

@section('content')
    <div class="row" v-if="pizzas.length" id="app">
        <pizza-item v-for="p in pizzas" :key="pizzas.id" v-bind:pizza="p" v-on:order="order($event)"></pizza-item>
    </div>
@endsection

@section('js')
    var app = new Vue({
        el: '#app',
        data: {
            pizzas: @json($pizzas)
        },
        methods: {
            order: function(id) {
                location.href = '{{ route('pizza.show',['id' => null]) }}' + '/' + id;
            }
        }
    })
@endsection
