<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('pizzas');
})->name('home');

Route::group(['prefix' => 'pizzas'], function() {
    Route::get('/', ['as' => 'pizzas', 'uses' => 'PizzaController@index']);
    Route::get('/{id}', ['as' => 'pizza.show', 'uses' => 'PizzaController@show']);
});

Route::group(['prefix' => 'order'], function() {
    Route::post('/', ['as' => 'order.store', 'uses' => 'OrderController@store']);
});