Vue.component('pizza-item',{
  props:['pizza'],
  template:`
    <div class="col-md-4">
      <div class="card" style="width: 18rem;">
        <img v-bind:src="'img/' + pizza.image" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">{{ pizza.name }}</h5>
          <p class="card-text">{{ pizza.description }}</p>
          <button class="btn btn-outline-primary btn-block" v-on:click="$emit('order', pizza.id)">Order Now</button>
        </div>
      </div>
    </div>
  `
});
Vue.component('pizza-detail', {
  props:['pizza','ingredientes'],
  template:`
    <div class="card mb-3">
      <div class="row no-gutters">
        <div class="col-md-4">
          <img v-bind:src="imageUrl" class="card-img-top" alt="...">
        </div>
        <div class="col-md-4">
          <div class="card-body">
            <h5 class="card-title">{{ pizza.name }}</h5>
            <p class="card-text">{{ pizza.description }}</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card-body">
            <h1 class="card-title">TOTAL</h1>
            <h2 class="card-text font-weight-bold font-italic text-secondary">$ {{ totalPrice }}</h2>
            <button type="button" class="btn btn-block btn-outline-success align-bottom" @click="$emit('show_ingredients')">Add Ingredient</button>
            <button type="button" class="btn btn-block btn-outline-primary align-bottom" @click="$emit('buy')">Buy Now</button>
          </div>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-md-12">
          <div class="card-body">
            <ul class="list-group list-group-flush">
              <li class="list-group-item-dark list-group-item d-flex justify-content-between align-items-center">
                Ingredients
                <span class="badge badge-dark badge-pill">{{ ingredientsCounter }}</span>
              </li>
            </ul>
            <ul class="list-group list-group-flush">
              <li v-for="(ingrediente, key) in ingredientes" class="list-group-item" style="padding:0;margin:0;">
                <div class="input-group" style="cursor:pointer;">
                  <div class="input-group-prepend" v-on:click="$emit('remove_ingredient', key)">
                    <span class="input-group-text alert-danger"><i class="fas fa-trash-alt"></i></span>
                  </div>
                  <input type="text" class="form-control bg-light" :value="ingrediente.name" disabled>
                  <div class="input-group-append">
                    <span class="input-group-text">$ {{ ingrediente.price }}</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  `,
  computed: {
    imageUrl: function() {
      return "../img/" +  this.pizza.image;
    },
    ingredientsCounter: function() {
      return this.ingredientes.length;
    },
    totalPrice: function() {
      total = 0;
      this.ingredientes.forEach(function(element, index, array) {
        total = total + parseFloat(element.price);
      });
      total = total * 1.5;    
      return total.toFixed(2);
    }
  }
});
