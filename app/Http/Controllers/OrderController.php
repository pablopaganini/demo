<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request) {
        $request->session()->push('orders', [
            'pizzaId' => $request->input('pizzaId'),
            'ingredientes' => $request->input('ingredientes')
        ]);
    }
}
