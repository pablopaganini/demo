<?php

namespace App\Http\Controllers;

use App\Pizza;
use App\Ingredient;
use Illuminate\Http\Request;

class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome', ['pizzas' => Pizza::all()]);
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(preg_match('/^[1-9]\d*$/', $id))
                return view('pizza', ['pizza' => Pizza::findOrFail($id), 'ingredients' => Ingredient::all()]);
            else
                throw new \Exception('Invalid id');
        }
        catch(\Exception $ex) {
            abort(404);
        }
    }
}
