<?php

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->insert([
            ['name' => 'tomato', 'price' => 0.5, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'sliced mushrooms', 'price' => 0.5, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'feta cheese', 'price' => 1, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'sausages', 'price' => 1, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'sliced onion', 'price' => 0.5, 'created_at' => date('Y-m-d H:i:s')],            
            ['name' => 'muzzarela cheese', 'price' => 1, 'created_at' => date('Y-m-d H:i:s')],            
            ['name' => 'oregano', 'price' => 0.5, 'created_at' => date('Y-m-d H:i:s')],            
            ['name' => 'bacon', 'price' => 1, 'created_at' => date('Y-m-d H:i:s')],            
        ]);
    }
}
