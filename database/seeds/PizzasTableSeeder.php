<?php

use Illuminate\Database\Seeder;

class PizzasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizzas')->insert([
            ['name' => 'Fun Pizza', 'description' => 'The most fun pizza ever made', 'image' => 'fun.jpg', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Super Mushrooms Pizza', 'description' => 'Great mushrooms pizza', 'image' => 'mush.jpeg', 'created_at' => date('Y-m-d H:i:s')],
        ]);
    }
}
