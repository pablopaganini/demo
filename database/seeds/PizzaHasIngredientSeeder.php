<?php

use Illuminate\Database\Seeder;

class PizzaHasIngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizza_has_ingredient')->insert([
            ['pizza_id' => 1, 'ingredient_id' => 1],
            ['pizza_id' => 1, 'ingredient_id' => 2],
            ['pizza_id' => 1, 'ingredient_id' => 3],
            ['pizza_id' => 1, 'ingredient_id' => 4],
            ['pizza_id' => 1, 'ingredient_id' => 5],
            ['pizza_id' => 1, 'ingredient_id' => 6],
            ['pizza_id' => 1, 'ingredient_id' => 7],            
            ['pizza_id' => 2, 'ingredient_id' => 1],            
            ['pizza_id' => 2, 'ingredient_id' => 8],            
            ['pizza_id' => 2, 'ingredient_id' => 6],            
            ['pizza_id' => 2, 'ingredient_id' => 2],            
            ['pizza_id' => 2, 'ingredient_id' => 7],            
        ]);
    }
}
